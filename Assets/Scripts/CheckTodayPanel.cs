﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class CheckTodayPanel : MonoBehaviour
{
	public Transform goalHolder;
	public GameObject todaygoalPrefab;
	Dictionary<string, GameObject> goalObjects = new Dictionary<string, GameObject>();

	//public 
	public delegate void CheckTodayHandler();
	public event CheckTodayHandler CheckFinished;

	public delegate void GoalStatusHandler(string id, bool completed);
	public event GoalStatusHandler GoalStatusChanged;

	public void SpawnGoals(Dictionary<string, Goal> goals)
	{
		ClearList();

		//Debug.Log("here");
		//Debug.Log(habits.Count);
		foreach (var goal in goals)
		{
			string x = goal.Key;
			//var pa = new RecurrencePattern(hab.Key.RepititionRules[0]);
			//Debug.Log(pa.ToString());
			//foreach (var period in goal.Value)
			//{
				//string time = goal.Value.d.StartTime.Hour.ToString("D2") + ":" + period.StartTime.Minute.ToString("D2");
				//string time = pa.ByHour[0].ToString("D2") + ":" + pa.ByMinute[0].ToString("D2");
				GameObject g = Instantiate(todaygoalPrefab, goalHolder);
			//g.GetComponent<TodayGoal>().message.text = g.GetComponent<TodayGoal>().question.Replace("***", goal.Value.Name);
			g.GetComponent<TodayGoal>().text.text = goal.Value.Name + "     " + goal.Value.DueDate.ToShortDateString();
				g.GetComponent<Image>().color = goal.Value.GoalColor;

				if (goalObjects.ContainsKey(x))
				{
				goalObjects[x] = g;
				}
				else
				{
					goalObjects.Add(x, g );
				}

			g.GetComponent<TodayGoal>().completed.onValueChanged.AddListener((bool b) => OnToggleClicked(x, b));
			g.GetComponent<TodayGoal>().completed.isOn = goal.Value.Completed;
			
			//g.GetComponent<TodayGoal>().noButton.onClick.AddListener(() => OnNoClicked(x));

				//MainMenu.instance.RegisterCheckInHabit(habit.GetComponent<TodayGoal>(), x, period.StartTime.Value);
			}
		}

	//void OnNoClicked(string i)
	//{
	//	Destroy(goalObjects[i]);
	//	if (goalObjects[i] == null)
	//	{
	//		goalObjects.Remove(i);
	//	}

	//	if (goalObjects.Count == 0)
	//	{
	//		if (CheckFinished != null)
	//		{
	//			CheckFinished();
	//		}
	//	}
	//}

	void OnToggleClicked(string i, bool v)
	{
		Debug.Log("here");
		//Destroy(goalObjects[i]);
		if (goalObjects[i] == null)
		{
			goalObjects.Remove(i);
		}

		if (goalObjects.Count == 0)
		{
			if (CheckFinished != null)
			{
				CheckFinished();
			}
		}

		if (GoalStatusChanged != null)
		{
			GoalStatusChanged(i, v);
		}
	}

	public void ClearList()
	{
		List<string> keys = goalObjects.Keys.ToList();

		for (int i = 0; i < keys.Count; i++)
		{
			//avoid out of range errors
			//for (int x = goalObjects[keys[i]].Count - 1; x > -1; x--)
			//{
				Destroy(goalObjects[keys[i]]);
			//}
		}

		keys.Clear();
		goalObjects.Clear();
	}
}
