﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TodayGoal : MonoBehaviour
{
	public Toggle completed;
	public TextMeshProUGUI text;
	public Button deleteButton;

	private void Awake()
	{
		completed.onValueChanged.AddListener(OnValueChanged);
	}

	protected void OnValueChanged(bool v)
	{
		if (v)
		{
			text.fontStyle = (FontStyles)66;
			text.color = Color.gray;
		}
		else
		{
			text.fontStyle = FontStyles.Normal;
			text.color = Color.black;
		}
		
	}
}
