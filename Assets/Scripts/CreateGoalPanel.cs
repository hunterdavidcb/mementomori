﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreateGoalPanel : MonoBehaviour
{
	public Button colorButton;
	public Button registerGoalButton;
	public InputField nameInput;
	public InputField descriptionInput;
	public Dropdown goalTypeDropdown;
	public Text errorText;
	public ColorPickerTriangle colorPicker;
	public Button acceptColorButton;
	public Button byCalendarButton;
	public GameObject colorBlocker;

	public GameObject dueDateEntryHolder;
	public GameObject calendarPanel;

	InputField dueDateYearInput, dueDateMonthInput, dueDateDayInput;

	Color goalColor;

	public delegate void GoalHandler(Goal g);
	public event GoalHandler GoalCreated;

	private void OnEnable()
	{
		colorButton.onClick.AddListener(OnColorPickerButtonCliked);
		acceptColorButton.onClick.AddListener(OnColorPicked);

		byCalendarButton.onClick.AddListener(OnCalendarClicked);
	}

	private void OnCalendarClicked()
	{
		//activate the calendar panel
		calendarPanel.SetActive(true);
		//activiate the monthly panel
		calendarPanel.GetComponent<CalendarPanel>().
			monthlyPanel.SetActive(true);

		//this is so fucking cool!!!!
		MonthPanel.MonthDayHandler monthdayCallback = new MonthPanel.MonthDayHandler(MonthDayCallbackFuntion);
		//initialize the month
		MonthPanel monthPanel = calendarPanel.GetComponent<CalendarPanel>().
			monthlyPanel.GetComponent<MonthPanel>();
		monthPanel.InitializeMonth(DateTime.Today.Year, DateTime.Today.Month, monthdayCallback);
		monthPanel.previousMonthButton.onClick.RemoveAllListeners();
		monthPanel.nextMonthButton.onClick.RemoveAllListeners();

		//set up the listeners
		monthPanel.previousMonthButton.onClick.AddListener(delegate
		{ OnPreviousMonthButtonPressed(DateTime.Today.Month, DateTime.Today.Year); });
		monthPanel.nextMonthButton.onClick.AddListener(delegate
		{ OnNextMonthButtonPressed(DateTime.Today.Month, DateTime.Today.Year); });
	}

	private void MonthDayCallbackFuntion(int year, int month, int day)
	{
		//close the calendar panel
		calendarPanel.SetActive(false);
		//close the month panel
		calendarPanel.GetComponent<CalendarPanel>().
			monthlyPanel.SetActive(false);

		// check validity of date
		if (day <= DateTime.DaysInMonth(year, month) && day > 0)
		{
			//set values of start date input
			dueDateYearInput.text = year.ToString();
			dueDateMonthInput.text = month.ToString();
			dueDateDayInput.text = day.ToString();
		}
		else
		{
			//show the error message
			errorText.gameObject.SetActive(true);
			errorText.text = "Please pick a valid due date";
			return;
		}

		//throw new NotImplementedException();
	}

	private void OnNextMonthButtonPressed(int currentMonth, int currentYear)
	{
		MonthPanel monthPanel = calendarPanel.GetComponent<CalendarPanel>().
			monthlyPanel.GetComponent<MonthPanel>();
		monthPanel.nextMonthButton.onClick.RemoveAllListeners();

		//do sanity check on month number
		int nextYear = currentMonth < 12 ? currentYear : currentYear + 1;
		int nextMonth = currentMonth < 12 ? currentMonth + 1 : 1;
		//Debug.Log(nextYear);
		//Debug.Log(nextMonth);

		monthPanel.nextMonthButton.onClick.AddListener(delegate
		{ OnNextMonthButtonPressed(nextMonth, nextYear); });

		//remember to update the previous month button
		monthPanel.previousMonthButton.onClick.RemoveAllListeners();
		monthPanel.previousMonthButton.onClick.AddListener(delegate
		{ OnPreviousMonthButtonPressed(nextMonth, nextYear); });


		//update the text
		monthPanel.InitializeMonth(nextYear, nextMonth);
	}

	private void OnPreviousMonthButtonPressed(int currentMonth, int currentYear)
	{
		MonthPanel monthPanel = calendarPanel.GetComponent<CalendarPanel>().
			monthlyPanel.GetComponent<MonthPanel>();
		monthPanel.previousMonthButton.onClick.RemoveAllListeners();

		//do sanity check on month number
		int previousYear = currentMonth > 1 ? currentYear : currentYear - 1;
		int previousMonth = currentMonth > 1 ? currentMonth - 1 : 12;

		//Debug.Log(previousYear);
		//Debug.Log(previousMonth);

		monthPanel.previousMonthButton.onClick.AddListener(delegate
		{ OnPreviousMonthButtonPressed(previousMonth, previousYear); });

		//remember to update the next month button
		monthPanel.nextMonthButton.onClick.RemoveAllListeners();
		monthPanel.nextMonthButton.onClick.AddListener(delegate
		{ OnNextMonthButtonPressed(previousMonth, previousYear); });


		//update the text
		monthPanel.InitializeMonth(previousYear, previousMonth);
	}

	protected void OnColorPickerButtonCliked()
	{
		//activate color picker
		colorBlocker.SetActive(true);
		colorPicker.gameObject.SetActive(true);
	}

	protected void OnColorPicked()
	{
		colorButton.image.color = colorPicker.TheColor;
		goalColor = colorPicker.TheColor;
		colorBlocker.SetActive(false);
		colorPicker.gameObject.SetActive(false);
	}

	// Start is called before the first frame update
	void Start()
    {
		colorPicker.gameObject.SetActive(false);
		colorBlocker.SetActive(false);

		errorText.gameObject.SetActive(false);

		dueDateYearInput = dueDateEntryHolder.transform.GetChild(0).GetComponent<InputField>();
		dueDateMonthInput = dueDateEntryHolder.transform.GetChild(1).GetComponent<InputField>();
		dueDateDayInput = dueDateEntryHolder.transform.GetChild(2).GetComponent<InputField>();

		//check to make sure this works!!!
		dueDateYearInput.onValueChanged.AddListener(delegate {
			OnInputValueChanged(4, dueDateYearInput.text, dueDateYearInput, dueDateMonthInput);
		});

		dueDateMonthInput.onValueChanged.AddListener(delegate {
			OnInputValueChanged(2, dueDateMonthInput.text, dueDateMonthInput, dueDateDayInput);
		});

		dueDateDayInput.onValueChanged.AddListener(delegate {
			OnInputValueChanged(2, dueDateDayInput.text, dueDateDayInput, null);
		});

		List<string> options = new List<string>();
		foreach (var item in Enum.GetValues(typeof(GoalCalendarType)))
		{
			options.Add(item.ToString());
		}

		goalTypeDropdown.ClearOptions();
		goalTypeDropdown.AddOptions(options);

		registerGoalButton.onClick.AddListener(CreateGoal);
	}

	protected void OnInputValueChanged(int length, string text, InputField current, InputField next)
	{
		Debug.Log("received");
		if (text.Length == length && next != null)
		{
			current.DeactivateInputField();
			next.ActivateInputField();
		}
		else if (text.Length == length)
		{
			current.DeactivateInputField();
		}
	}

	// Update is called once per frame
	void Update()
    {
        
    }

	bool IsValidDueDate()
	{
		int year, month, day;
		Int32.TryParse(dueDateYearInput.text, out year);
		if (year >= 0)
		{
			Int32.TryParse(dueDateMonthInput.text, out month);
			if (month >= 1 && month <= 12)
			{
				Int32.TryParse(dueDateDayInput.text, out day);
				if (day >= 1 && day <= DateTime.DaysInMonth(year, month))
				{
					return true;
				}
			}
		}

		return false;
	}

	bool IsValidName()
	{
		return !nameInput.text.Equals("");
	}

	bool IsValidDescription()
	{
		return !descriptionInput.text.Equals("");
	}

	public void OnDisable()
	{
		((Text)nameInput.placeholder).text = "Enter goal name...";
		nameInput.text = "";

		((Text)descriptionInput.placeholder).text = "Enter goal description...";
		descriptionInput.text = "";

		((Text)dueDateYearInput.placeholder).text = "Enter year";
		dueDateYearInput.text = "";

		((Text)dueDateMonthInput.placeholder).text = "Enter month";
		dueDateMonthInput.text = "";

		((Text)dueDateDayInput.placeholder).text = "Enter day";
		dueDateDayInput.text = "";

		

		errorText.text = "";
		errorText.gameObject.SetActive(false);

		colorButton.onClick.RemoveAllListeners();
		acceptColorButton.onClick.RemoveAllListeners();

		colorPicker.TheColor = new Color(0f, 0f, 0f, 0f);
	}

	public void CreateGoal()
	{
		if (!IsValidName())
		{
			errorText.gameObject.SetActive(true);
			errorText.text = "Please enter a name";
			return;
		}

		if (!IsValidDueDate())
		{
			errorText.gameObject.SetActive(true);
			errorText.text = "Due date is not valid";
			return;
		}

		

		if (goalColor.Equals(new Color(0f, 0f, 0f, 0f)))
		{
			errorText.gameObject.SetActive(true);
			errorText.text = "Pick a color";
			return;
		}

		errorText.gameObject.SetActive(false);

		
		DateTime start = new DateTime(Int32.Parse(dueDateYearInput.text),
			Int32.Parse(dueDateMonthInput.text), Int32.Parse(dueDateDayInput.text));




		Goal g = new Goal(nameInput.text, descriptionInput.text, start, (GoalCalendarType)goalTypeDropdown.value, goalColor);

		Debug.Log(g);

		if (GoalCreated != null)
		{
			GoalCreated(g);
		}

		
	}
}
