﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class UserData 
{
	string name;
	DateTime birthday;
	DateTime lastLogin;
	DateTime death;
	string country;
	Gender gender;
	Dictionary<string, Goal> goals;

	public string Name
	{
		get { return name; }
	}

	public string Country
	{
		get { return country; }
	}

	public Gender Gender
	{
		get { return gender; }
	}

	public DateTime Birthday
	{
		get { return birthday; }
	}

	public DateTime Death
	{
		get { return death; }
	}

	public DateTime LastLogin
	{
		get { return lastLogin; }
		set { lastLogin = value; }
	}

	public Dictionary<string, Goal> Goals
	{
		get { return goals; }
	}

	public UserData()
	{
		goals = new Dictionary<string, Goal>();
		lastLogin = DateTime.MinValue;
	}

	public UserData(string n, string c, DateTime bd, DateTime dd, Gender g)
	{
		name = n;
		country = c;
		birthday = bd;
		death = dd;
		gender = g;
		goals = new Dictionary<string, Goal>();
		lastLogin = DateTime.MinValue;
	}

	public void UpdateGoalStatus(string id, bool status)
	{
		if (goals.ContainsKey(id))
		{
			goals[id].SetCompleted(status);
		}
	}

	public void UpdateGoalDate(string id, DateTime d)
	{
		if (goals.ContainsKey(id))
		{
			goals[id].SetDueDate(d);
		}
	}

	public void UpdateGoalDescription(string id, string d)
	{
		if (goals.ContainsKey(id))
		{
			goals[id].SetDescription(d);
		}
	}

	public void UpdateGoalColor(string id, Color c)
	{
		if (goals.ContainsKey(id))
		{
			goals[id].SetColor(c);
		}
	}

	public void AddGoal(Goal g)
	{
		if (!goals.ContainsKey(g.ID))
		{
			goals.Add(g.ID, g);
		}
	}

	public void RemoveGoal(Goal g)
	{
		if (goals.ContainsKey(g.ID))
		{
			goals.Remove(g.ID);
		}
	}

	public void ClearGoals()
	{
		goals.Clear();
		goals = new Dictionary<string, Goal>();
	}
}
