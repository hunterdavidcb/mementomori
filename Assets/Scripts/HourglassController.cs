﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HourglassController : MonoBehaviour
{
	public Image top;
	public Image bottom;
	public GameObject piecePref;

	//for testing
	[Range(1960,2017)]
	public int bYear;
	[Range(1, 12)]
	public int bMonth;
	[Range(1, 31)]
	public int bDay;

	//for testing
	[Range(1960, 2100)]
	public int dYear;
	[Range(1, 12)]
	public int dMonth;
	[Range(1, 31)]
	public int dDay;
	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	public void AdjustPercentages(DateTime birthday, DateTime death, DateTime lastLogin)
	{
		TimeSpan todayToBirth = DateTime.Today - birthday;
		TimeSpan deathToToday = death - DateTime.Today;

		float tt = (float)deathToToday.Days / (todayToBirth.Days + deathToToday.Days);
		float bt = (float)todayToBirth.Days / (todayToBirth.Days + deathToToday.Days);
		//top.fillAmount = (float)deathToToday.Days / (todayToBirth.Days + deathToToday.Days);
		//bottom.fillAmount = (float)todayToBirth.Days / (todayToBirth.Days + deathToToday.Days);

		

		int numDays = (DateTime.Today - lastLogin).Days;
		//Debug.Log(numDays);
		StartCoroutine(ChangeFillAmounts(tt, bt,Mathf.Max(numDays-10,1)));
		StartCoroutine(SpawnPieces(numDays));
	}

	IEnumerator ChangeFillAmounts(float targetTop, float targetBottom, float time)
	{
		yield return new WaitForSeconds(2f);
		float t = 0;
		float topOrig = top.fillAmount;
		float bottomOrig = bottom.fillAmount;
		while (t < time)
		{
			t += Time.deltaTime;
			top.fillAmount = Mathf.Lerp(topOrig, targetTop, t / time);
			bottom.fillAmount = Mathf.Lerp(bottomOrig, targetTop, t / time);
			yield return null;
		}
	}

	IEnumerator SpawnPieces(int number)
	{
		//spawn the pieces
		// with a delay between them
		yield return new WaitForSeconds(2f);
		for (int i = 0; i < number; i++)
		{
			GameObject p = Instantiate(piecePref.gameObject, transform);
			//move them one by one
			//Debug.Log(p.transform.position);
			StartCoroutine(MovePiece(p));
			yield return new WaitForSeconds(.5f);
		}


	
		yield return null;
	}

	IEnumerator MovePiece(GameObject go)
	{
		
		RectTransform rect = go.GetComponent<RectTransform>();
		//Debug.Log(rect.anchoredPosition);
		while (rect.anchoredPosition.y > -200 )
		{
			rect.anchoredPosition += Vector2.down;
			yield return null;
		}
		Destroy(go);
	}
}
