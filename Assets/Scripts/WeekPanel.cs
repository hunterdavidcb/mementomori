﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class WeekPanel : MonoBehaviour
{
	public Text weekInfo;
	public GameObject mondayHolder;
	public GameObject tuesdayHolder;
	public GameObject wednesdayHolder;
	public GameObject thursdayHolder;
	public GameObject fridayHolder;
	public GameObject saturdayHolder;
	public GameObject sundayHolder;

	public Button previousWeekButton, nextWeekButton;

	public GameObject habitPrefab;

	Dictionary<string, List<GameObject>> goalObjects = new Dictionary<string, List<GameObject>>();
	Dictionary<GameObject, List<GameObject>> weekdayGoals = new Dictionary<GameObject, List<GameObject>>();

	DateTime weekStart;

	public delegate void DayClickHandler(DateTime dateTime);
	public event DayClickHandler DayClicked;

	public delegate void HabitClickHandler(string id);
	public event HabitClickHandler HabitClicked;

	private void Awake()
	{
		//weekStart
		weekdayGoals.Add(mondayHolder, new List<GameObject>());
		weekdayGoals.Add(tuesdayHolder, new List<GameObject>());
		weekdayGoals.Add(wednesdayHolder, new List<GameObject>());
		weekdayGoals.Add(thursdayHolder, new List<GameObject>());
		weekdayGoals.Add(fridayHolder, new List<GameObject>());
		weekdayGoals.Add(saturdayHolder, new List<GameObject>());
		weekdayGoals.Add(sundayHolder, new List<GameObject>());
	}

	public void SpawnHabits(Dictionary<string, Goal> goals)
	{
		ClearList();
		foreach (var goal in goals)
		{
			
				switch (goal.Value.DueDate.DayOfWeek)
				{
					case DayOfWeek.Friday:
						if (weekdayGoals[fridayHolder].Count < 6)
						{
							//it is safe to add to the list

							//we should check to see about the time order 
							//how should we do this???
							GameObject habitObject = Instantiate(habitPrefab, fridayHolder.transform);

							AddHabitObject(fridayHolder, habitObject, goal.Value);

						}
						else
						{
							//add a day preview expansion button (+)
						}

						fridayHolder.GetComponentInParent<Button>().onClick.RemoveAllListeners();
						fridayHolder.GetComponentInParent<Button>().onClick.AddListener(
							delegate { OnDayClicked(goal.Value.DueDate); });
						break;
					case DayOfWeek.Monday:
						if (weekdayGoals[mondayHolder].Count < 6)
						{
							//it is safe to add to the list

							//we should check to see about the time order 
							//how should we do this???
							GameObject habitObject = Instantiate(habitPrefab, mondayHolder.transform);

							AddHabitObject(mondayHolder, habitObject, goal.Value);
						}
						else
						{
							//add a day preview expansion button (+)
						}

						mondayHolder.GetComponentInParent<Button>().onClick.RemoveAllListeners();
						mondayHolder.GetComponentInParent<Button>().onClick.AddListener(
							delegate { OnDayClicked(goal.Value.DueDate); });
						break;
					case DayOfWeek.Saturday:
						if (weekdayGoals[saturdayHolder].Count < 6)
						{
							//it is safe to add to the list

							//we should check to see about the time order 
							//how should we do this???
							GameObject habitObject = Instantiate(habitPrefab, saturdayHolder.transform);

							AddHabitObject(saturdayHolder, habitObject, goal.Value);
						}
						else
						{
							//add a day preview expansion button (+)
						}
						saturdayHolder.GetComponentInParent<Button>().onClick.RemoveAllListeners();
						saturdayHolder.GetComponentInParent<Button>().onClick.AddListener(
							delegate { OnDayClicked(goal.Value.DueDate); });

						break;
					case DayOfWeek.Sunday:
						if (weekdayGoals[sundayHolder].Count < 6)
						{
							//it is safe to add to the list
							//Debug.Log("adding sunday");
							//we should check to see about the time order 
							//how should we do this???
							GameObject habitObject = Instantiate(habitPrefab, sundayHolder.transform);

							AddHabitObject(sundayHolder, habitObject, goal.Value);
						}
						else
						{
							//add a day preview expansion button (+)
						}
						sundayHolder.GetComponentInParent<Button>().onClick.RemoveAllListeners();
						sundayHolder.GetComponentInParent<Button>().onClick.AddListener(
							delegate { OnDayClicked(goal.Value.DueDate); });
						break;
					case DayOfWeek.Thursday:
						if (weekdayGoals[thursdayHolder].Count < 6)
						{
							//it is safe to add to the list

							//we should check to see about the time order 
							//how should we do this???
							GameObject habitObject = Instantiate(habitPrefab, thursdayHolder.transform);

							AddHabitObject(thursdayHolder, habitObject, goal.Value);
						}
						else
						{
							//add a day preview expansion button (+)
						}
						thursdayHolder.GetComponentInParent<Button>().onClick.RemoveAllListeners();
						thursdayHolder.GetComponentInParent<Button>().onClick.AddListener(
							delegate { OnDayClicked(goal.Value.DueDate); });
						break;
					case DayOfWeek.Tuesday:
						if (weekdayGoals[tuesdayHolder].Count < 6)
						{
							//it is safe to add to the list

							//we should check to see about the time order 
							//how should we do this???
							GameObject habitObject = Instantiate(habitPrefab, tuesdayHolder.transform);

							AddHabitObject(tuesdayHolder, habitObject, goal.Value);
						}
						else
						{
							//add a day preview expansion button (+)
						}
						tuesdayHolder.GetComponentInParent<Button>().onClick.RemoveAllListeners();
						tuesdayHolder.GetComponentInParent<Button>().onClick.AddListener(
							delegate { OnDayClicked(goal.Value.DueDate); });
						break;
					case DayOfWeek.Wednesday:
						if (weekdayGoals[wednesdayHolder].Count < 6)
						{
							//it is safe to add to the list

							//we should check to see about the time order 
							//how should we do this???
							GameObject habitObject = Instantiate(habitPrefab, wednesdayHolder.transform);

							AddHabitObject(wednesdayHolder, habitObject, goal.Value);
						}
						else
						{
							//add a day preview expansion button (+)
						}
						wednesdayHolder.GetComponentInParent<Button>().onClick.RemoveAllListeners();
						wednesdayHolder.GetComponentInParent<Button>().onClick.AddListener(
							delegate { OnDayClicked(goal.Value.DueDate); });
						break;
					default:
						break;
				}
		}
	}

	public void ClearList()
	{
		List<string> keys = goalObjects.Keys.ToList();

		for (int i = 0; i < keys.Count; i++)
		{
			//avoid out of range errors
			for (int x = goalObjects[keys[i]].Count - 1; x > -1; x--)
			{
				Destroy(goalObjects[keys[i]][x]);
				//Debug.Log("destroying");
			}
		}

		keys.Clear();
		goalObjects.Clear();
		foreach (var day in weekdayGoals)
		{
			day.Value.Clear();
		}
	}

	void AddHabitObject(GameObject holder, GameObject goal, Goal go)
	{
		weekdayGoals[holder].Add(goal);
		if (!goalObjects.ContainsKey(go.ID))
		{
			goalObjects.Add(go.ID, new List<GameObject>() { goal });
		}
		else
		{
			goalObjects[go.ID].Add(goal);
		}
		goal.GetComponent<Image>().color = go.GoalColor;
		goal.GetComponent<Button>().onClick.AddListener(delegate { OnHabitClicked(go.ID); });
	}

	protected void OnHabitClicked(string id)
	{
		if (HabitClicked != null)
		{
			Debug.Log("calling");
			HabitClicked(id);
		}
	}

	protected void OnDayClicked(DateTime dateTime)
	{
		if (DayClicked != null)
		{
			DayClicked(dateTime);
		}
	}

	public static DateTime FirstDateOfWeek(string week)
	{
		char[] splitter = new char[] { '-'};
		int year = int.Parse(week.Split(splitter)[1]);
		int weekNumber = int.Parse(week.Split(splitter)[0].Remove(0));

		DateTime jan1 = new DateTime(year, 1, 1);
		int daysOffset = DayOfWeek.Thursday - jan1.DayOfWeek;

		DateTime firstThursday = jan1.AddDays(daysOffset);
		var cal = CultureInfo.CurrentCulture.Calendar;

		int firstWeek = cal.GetWeekOfYear(firstThursday, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

		var weekNum = weekNumber;
		if (firstWeek == 1)
		{
			weekNum -= 1;
		}

		var result = firstThursday.AddDays(weekNum * 7);


		return result.AddDays(-3);
	}

	public static DateTime FirstDateOfWeek(int weekNumber, int year)
	{
		
		DateTime jan1 = new DateTime(year, 1, 1);
		int daysOffset = DayOfWeek.Thursday - jan1.DayOfWeek;

		DateTime firstThursday = jan1.AddDays(daysOffset);
		var cal = CultureInfo.CurrentCulture.Calendar;

		int firstWeek = cal.GetWeekOfYear(firstThursday, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

		var weekNum = weekNumber;
		if (firstWeek == 1)
		{
			weekNum -= 1;
		}


		var result = firstThursday.AddDays(weekNum * 7);


		return result.AddDays(-3);
	}

	public static int WeekNumber(DateTime date)
	{
		var cal = CultureInfo.CurrentCulture.Calendar;

		return cal.GetWeekOfYear(date, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

	}
}
