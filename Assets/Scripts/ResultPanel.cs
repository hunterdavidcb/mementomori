﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultPanel : MonoBehaviour
{
	public Button moveOn;
	public Text message;
	public Text quote;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	public void Play()
	{
		GetComponent<Animator>().SetTrigger("Play");
	}
}
