﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DayPanel : MonoBehaviour
{
	public Text dayInfo;
	public GameObject hoursHolder;
	public GameObject dayHabitPrefab;
	Dictionary<string, GameObject> goalObjects = new Dictionary<string, GameObject>();
	//Dictionary<int, GameObject> hoursDictionary = new Dictionary<int, GameObject>();

	public Button previousDayButton, nextDayButton;

	public delegate void GoalClickHandler(string id, bool status);
	public event GoalClickHandler GoalClicked;

	public delegate void GoalRemovalHandler(string id);
	public event GoalRemovalHandler GoalRemoved;

	public void SpawnGoals(Dictionary<string, Goal> goals)
	{
		ClearList();
		foreach (var goal in goals)
		{
			string x = goal.Key;


			GameObject goalObject = Instantiate(dayHabitPrefab, hoursHolder.transform);
			goalObject.GetComponentInChildren<TextMeshProUGUI>().text = goal.Value.Name;
			goalObject.GetComponent<TodayGoal>().completed.isOn = goal.Value.Completed;
			goalObject.GetComponent<TodayGoal>().completed.onValueChanged.AddListener((bool b) => { GoalClicked(goal.Key, b); });
			goalObject.GetComponent<TodayGoal>().deleteButton.onClick.AddListener(() => { GoalRemoved(goal.Key); });
			goalObject.GetComponent<Image>().color = goal.Value.GoalColor;

			
			goalObjects.Add(x,  goalObject);
		}
	}

	public void ClearList()
	{
		List<string> keys = goalObjects.Keys.ToList();

		for (int i = 0; i < keys.Count; i++)
		{
			
			Destroy(goalObjects[keys[i]]);
		}

		keys.Clear();
		goalObjects.Clear();
	}

}
