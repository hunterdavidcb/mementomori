﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuoteData 
{
	public string quote;
	public string author;
	public string theme;

	public override string ToString()
	{
		return quote + "\n ~" + author;
	}
}
