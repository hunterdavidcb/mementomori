﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MonthPanel : MonoBehaviour
{
	public Text monthInfo;
	public GameObject daysHolder;
	public GameObject habitPrefab;

	public Button previousMonthButton, nextMonthButton;

	Text[] daysOfMonth;// = new List<Text>;
	Dictionary<string, List<GameObject>> goalObjects = new Dictionary<string, List<GameObject>>();
	Dictionary<GameObject, List<GameObject>> weekdayGoals = new Dictionary<GameObject, List<GameObject>>();
	Dictionary<int, GameObject> daysOfMonthDictionary = new Dictionary<int, GameObject>();

	public delegate void MonthDayHandler(int year, int month, int day);
	public event MonthDayHandler MonthDayClicked;

	private void Awake()
	{
		//Debug.Log("running");
		daysOfMonth = daysHolder.GetComponentsInChildren<Text>();
		for (int i = 0; i < daysHolder.transform.childCount; i++)
		{
			weekdayGoals.Add(daysHolder.transform.GetChild(i).GetChild(1).gameObject, new List<GameObject>());
		}
	}

	public void InitializeMonth(int year, int month, MonthDayHandler mdh = null)
	{
		monthInfo.text = ((Months)month).ToString() + ", " + year.ToString();
		DateTime test = new DateTime(year, month, 1);

		daysOfMonthDictionary.Clear();
		//Debug.Log(daysOfMonth.Length);

		int startingPos = (test.DayOfWeek == DayOfWeek.Sunday ? (int)test.DayOfWeek + 6 : (int)test.DayOfWeek - 1);
		//deactivate the unneeded days
		for (int i = 0; i < startingPos; i++)
		{
			daysOfMonth[i].gameObject.SetActive(false);
			//Debug.Log(i);
			//Debug.Log(daysOfMonth[i].name);
			//Debug.Log(daysOfMonth[i].transform.parent.gameObject.name);
			daysOfMonth[i].transform.parent.GetComponent<Button>().onClick.RemoveAllListeners();
		}

		int counter = 1;
		for (int i = startingPos; i < DateTime.DaysInMonth(year,month) + startingPos; i++)
		{
			//activate the object
			daysOfMonth[i].gameObject.SetActive(true);

			//clear the listeners
			daysOfMonth[i].transform.parent.GetComponent<Button>().onClick.RemoveAllListeners();

			//set text to the correct number
			daysOfMonth[i].text = counter.ToString();

			//weird, but prevents out of range errors
			int day = counter;
			int y = year;
			int m = month;

			if (mdh == null)
			{
				daysOfMonth[i].transform.parent.GetComponent<Button>().onClick.AddListener(delegate { OnMonthDayClicked(y, m, day); });
			}
			else
			{
				daysOfMonth[i].transform.parent.GetComponent<Button>().onClick.AddListener(delegate { mdh(y, m, day); });
			}
			
			daysOfMonthDictionary.Add(counter, daysHolder.transform.GetChild(i).GetChild(1).gameObject);
			counter++;
		}

		//deactivate the extra days at the end
		for (int i = DateTime.DaysInMonth(year, month) + startingPos; i < daysOfMonth.Length ; i++)
		{
			daysOfMonth[i].gameObject.SetActive(false);
			daysOfMonth[i].transform.parent.GetComponent<Button>().onClick.RemoveAllListeners();
		}
		
	}

	protected void OnMonthDayClicked(int year, int month, int day)
	{
		if (MonthDayClicked != null)
		{
			MonthDayClicked(year, month, day);
		}
	}

	public void SpawnGoals(Dictionary<string, Goal> goals)
	{
		ClearList();
		foreach (var goal in goals)
		{
			
				//get the day of month and convert it to the proper range
				int index = goal.Value.DueDate.Day;
				//Debug.Log(daysOfMonthDictionary[index]);

				if (weekdayGoals[daysOfMonthDictionary[index]].Count < 4)
				{
					//we have a small enough number of habits to display
					//Debug.Log(daysOfMonthDictionary[index].name);
					GameObject goalObject = Instantiate(habitPrefab, daysOfMonthDictionary[index].transform);
					AddHabitObject(daysOfMonthDictionary[index], goalObject, goal.Value);
				}
				else
				{
					//we should show 
				}
		}
	}

	void AddHabitObject(GameObject holder, GameObject goal, Goal go)
	{
		weekdayGoals[holder].Add(goal);
		if (!goalObjects.ContainsKey(go.ID))
		{
			goalObjects.Add(go.ID, new List<GameObject>() { goal });
		}
		else
		{
			goalObjects[go.ID].Add(goal);
		}
		goal.GetComponentInChildren<Text>().text = go.Name;
		goal.GetComponent<Image>().color = go.GoalColor;
	}

	public void ClearList()
	{
		List<string> keys = goalObjects.Keys.ToList();

		for (int i = 0; i < keys.Count; i++)
		{
			//avoid out of range errors
			for (int x = goalObjects[keys[i]].Count - 1; x > -1; x--)
			{
				Destroy(goalObjects[keys[i]][x]);
			}
		}

		keys.Clear();
		goalObjects.Clear();
		foreach (var day in weekdayGoals)
		{
			day.Value.Clear();
		}
	}

}
