﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnSand : MonoBehaviour
{
	public GameObject sand;
	public Transform top;
	public Transform bottom;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		Instantiate(sand, transform.position, Quaternion.identity);
    }


}
