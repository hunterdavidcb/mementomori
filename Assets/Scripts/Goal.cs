﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Goal
{
	string id;
	string name;
	string description;
	bool completed;
	DateTime dueDate;
	GoalCalendarType goalType;
	float goalR, goalG, goalB;

	#region Constructors

	public Goal()
	{
		id = Guid.NewGuid().ToString();
		goalType = GoalCalendarType.Day;
	}

	public Goal(string n)
	{
		id = Guid.NewGuid().ToString();
		name = n;
		goalType = GoalCalendarType.Day;
	}

	public Goal(string n, string d)
	{
		id = Guid.NewGuid().ToString();
		name = n;
		description = d;
		goalType = GoalCalendarType.Day;
	}

	public Goal(string n, string d, DateTime dd)
	{
		id = Guid.NewGuid().ToString();
		name = n;
		description = d;
		dueDate = dd;
		goalType = GoalCalendarType.Day;
	}

	public Goal(string n, string d, DateTime dd, GoalCalendarType g)
	{
		id = Guid.NewGuid().ToString();
		name = n;
		description = d;
		dueDate = dd;
		goalType = g;
	}

	public Goal(string n, string d, DateTime dd, GoalCalendarType g, Color c)
	{
		id = Guid.NewGuid().ToString();
		name = n;
		description = d;
		dueDate = dd;
		goalType = g;
		goalR = c.r;
		goalG = c.g;
		goalB = c.b;
	}

	#endregion

	#region Properties

	public string Name
	{
		get { return name; }
	}

	public string ID
	{
		get { return id; }
	}

	public string Description
	{
		get { return description; }
	}

	public DateTime DueDate
	{
		get { return dueDate; }
	}

	public bool Completed
	{
		get { return completed; }
	}

	public Color GoalColor
	{
		get { return new Color(goalR, goalG, goalB); }
	}

	public GoalCalendarType GoalType
	{
		get { return goalType; }
	}

	#endregion

	#region Set Functions

	public void SetDescription(string d)
	{
		description = d;
	}

	public void SetDueDate(DateTime dd)
	{
		dueDate = dd;
	}

	public void SetCompleted(bool c)
	{
		completed = c;
	}

	public void SetColor(Color c)
	{
		goalR = c.r;
		goalG = c.g;
		goalB = c.b;
	}

	#endregion

	public override string ToString()
	{
		return name + "\n" + description + "\n" + dueDate.ToShortDateString() + "\n"
			+ completed.ToString();
	}
}
