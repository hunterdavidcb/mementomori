﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public static class SerializeUser
{
    public static void Serialize(UserData ud)
	{
		string path = Application.persistentDataPath + "/UserData.dat";
		Stream s = File.Open(path, FileMode.OpenOrCreate);
		BinaryFormatter bf = new BinaryFormatter();
		bf.Serialize(s, ud);
		s.Close();
	}

	public static UserData Deserialize()
	{
		
		string path = Application.persistentDataPath + "/UserData.dat";
		FileStream s = new FileStream(path, FileMode.OpenOrCreate);
		if (s.Length != 0)
		{
			BinaryFormatter bf = new BinaryFormatter();
			UserData ud = bf.Deserialize(s) as UserData;
			s.Close();
			return ud;
		}
		
		

		return null;
	}
}
