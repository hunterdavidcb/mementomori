﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ReadCSV
{
	public static Dictionary<string,CountryData> Load(string malePath, string femalePath)
	{
		Dictionary<string, CountryData> countries = new Dictionary<string, CountryData>();
		var fileData  = System.IO.File.ReadAllText(malePath);
		string[] lines = fileData.Split('\n');

		//Debug.Log(lines.Length);

		string[] headerLine = (lines[0].Trim()).Split(',');

		for (int i = 1; i < lines.Length; i++)
		{
			string[] lineData = (lines[i].Trim()).Split(',');
			//Debug.Log(lineData.Length);
			CountryData cd = new CountryData(lineData[0]);
			//Debug.Log(lineData[0]);
			Dictionary<int, float> lifeExpentancy = new Dictionary<int, float>();
			for (int x = 4; x < lineData.Length; x++)
			{
				
				if (x < headerLine.Length)
				{
					int year;
					if (int.TryParse(headerLine[x], out year))
					{
						float expect;
						if (float.TryParse(lineData[x], out expect))
						{
							//Debug.Log(year + " " + expect);
							lifeExpentancy.Add(year, expect);
						}
						else
						{
							//lifeExpentancy.Add(year, float.MinValue);
						}
					}
				}
			}

			cd.AddGenderData(Gender.Male, lifeExpentancy);
			countries.Add(lineData[0], cd);
		}

		fileData = System.IO.File.ReadAllText(femalePath);
		lines = fileData.Split('\n');

		headerLine = (lines[0].Trim()).Split(',');

		for (int i = 1; i < lines.Length; i++)
		{
			string[] lineData = (lines[i].Trim()).Split(',');
			CountryData cd = new CountryData(lineData[0]);
			Dictionary<int, float> lifeExpentancy = new Dictionary<int, float>();
			for (int x = 3; x < lineData.Length; x++)
			{

				if (x < headerLine.Length)
				{
					int year;
					if (int.TryParse(headerLine[x], out year))
					{
						float expect;
						if (float.TryParse(lineData[x], out expect))
						{
							lifeExpentancy.Add(year, expect);
						}
						else
						{
							lifeExpentancy.Add(year, float.MinValue);
						}
					}		
				}
			}

			cd.AddGenderData(Gender.Female, lifeExpentancy);
			if (countries.ContainsKey(lineData[0]))
			{
				countries[lineData[0]].AddGenderData(Gender.Female, lifeExpentancy);
			}
			else
			{
				countries.Add(lineData[0], cd);
			}
			
		}

		return countries;
	}

	public static List<QuoteData> ReadQuotes()
	{
		string datapath = Application.dataPath + "/CSV/Quotes.csv";
		var fileData = System.IO.File.ReadAllText(datapath);
		string[] lines = fileData.Split('\n');

		//Debug.Log(lines.Length);

		List<QuoteData> qd = new List<QuoteData>();
		for (int i = 0; i < lines.Length; i++)
		{
			string[] lineData = (lines[i].Trim()).Split(';');
			//Debug.Log(lineData.Length);
			//Debug.Log(lineData[0]);
			//Debug.Log(lineData[1]);
			//Debug.Log(lineData[2]);
			QuoteData q = new QuoteData();
			q.quote = lineData[0];
			q.author = lineData[1];
			q.theme = lineData[2];

			qd.Add(q);
		}

		return qd;
	}
}
