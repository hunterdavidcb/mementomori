﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CountryData 
{
	string name;
	Dictionary<Gender, Dictionary<int, float>> lifeExpectancyByGenderPerYear;

	public string Name
	{
		get { return name; }
	}

	public float GetExpectancy(Gender g, int year)
	{
		float expect;
		Dictionary<int, float> expectancy;
		if (lifeExpectancyByGenderPerYear.TryGetValue(g, out expectancy))
		{
			if (expectancy.TryGetValue(year, out expect))
			{
				return expect;
			}
		}

		return float.MinValue;
		
	}

	public List<int> GetYears(Gender g)
	{
		return lifeExpectancyByGenderPerYear[g].Keys.ToList();
	}

	public CountryData()
	{
		name = "Name Me";
		lifeExpectancyByGenderPerYear = new Dictionary<Gender, Dictionary<int, float>>();
	}

	public CountryData(string n)
	{
		name = n;
		lifeExpectancyByGenderPerYear = new Dictionary<Gender, Dictionary<int, float>>();
	}

	public void AddGenderData(Gender g, Dictionary<int, float> data)
	{
		if (!lifeExpectancyByGenderPerYear.ContainsKey(g))
		{
			lifeExpectancyByGenderPerYear.Add(g, data);
		}
	}

	public void ChangeName(string n)
	{
		name = n;
	}
}

public enum Gender
{
	Male,
	Female
}
