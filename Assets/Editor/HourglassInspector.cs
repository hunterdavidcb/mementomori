﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(HourglassController))]
[ExecuteInEditMode]
public class HourGlassInspector : Editor
{
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();

		if (GUILayout.Button("Calculate"))
		{
			Debug.Log("here");
			DateTime bDay = new DateTime((target as HourglassController).bYear,
				(target as HourglassController).bMonth, (target as HourglassController).bDay);
			DateTime dDay = new DateTime((target as HourglassController).dYear,
				(target as HourglassController).dMonth, (target as HourglassController).dDay);
			(target as HourglassController).AdjustPercentages(bDay,dDay,DateTime.Today);
		}
	}
}
